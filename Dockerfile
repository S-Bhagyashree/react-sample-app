FROM node:12.2.0 as build-stage

ARG REACT_APP_API_URL
ENV REACT_APP_API_URL $REACT_APP_API_URL

WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build


FROM nginx:1.13.12-alpine as production-stage
RUN mkdir -p /usr/share/nginx/html
RUN mkdir -p /usr/share/nginx/html/acme

COPY --from=build-stage /app/build /usr/share/nginx/html
COPY vhost.conf /etc/nginx/conf.d/
COPY mime.types /etc/nginx/mime.types

RUN rm /etc/nginx/conf.d/default.conf

# Configure timestamp
RUN rm /etc/localtime
RUN ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime

# Create file for acme challenge
RUN echo "acme-test" > /usr/share/nginx/html/acme/index.html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
